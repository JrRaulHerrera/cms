<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use MP;

use URL;
use Session;
use DB;
use App\pp_reservation;
use App\pp_reservation_details;


class mercadopagoController extends Controller
{

    public $CLIENT_ID="850785406949633";
    public $CLIENT_SECRET="L9Iv1YaKRvHZtuKCgAdrrq86fFFApYbj";

    public function postPayment(Request $request)//Metodo para enviar a Paypal
    {    
    if($request['total']==0 || $request['total']==null )
        return redirect('Inicio')//Si por alguna razon no hay un precio total para enviar a paypal 
        ->with('message', 'Lo sentimos, Reservaciones no disponible en estos momentos');

    $arrayItemToPay=unserialize($request['arrayItemToPay']);
    //$arrayItemToPay['total']= $request['total'];
    $num_hab=$arrayItemToPay["habitacion"];
    //dd($arrayItemToPay["habitacion"]);
    $arrayIdPricesHab=$request['precios'];
    
    /*for($i=0;$i<$num_hab;$i++){
        echo $arrayIdPricesHab[$i];
    }*/

     $arrayItemToPay;
     $arrayIdPricesHab;


      $price=$request['total'];
      $price=(float)$price;
     
      $name_client=$arrayItemToPay['nombre'];
      $llegada=$arrayItemToPay['llegada'];
      $salida=$arrayItemToPay['salida'];
      $habitacion=$arrayItemToPay['habitacion'];
      $adultos=$arrayItemToPay['adultos'];
      $menores=$arrayItemToPay['menores'];
      $promo=$arrayItemToPay['promo'];
    
     
      $name='Reservación';
      $extract='Reservacion en Hotel posada paraíso';
      $quantity='1';
      
     
      $costoDeEnvio=0;

      Session::put('nombre', $arrayItemToPay['nombre']);
      Session::put('llegada', $arrayItemToPay['llegada']);//Respaldo mis datos antes de enviar a la pagina depaypal(para no perderlos)
      Session::put('salida', $arrayItemToPay['salida']);
      Session::put('habitacion', $arrayItemToPay['habitacion']);
      Session::put('adultos', $arrayItemToPay['adultos']);
      Session::put('menores',$arrayItemToPay['menores']);
      Session::put('promo', $arrayItemToPay['promo']);
      Session::put('name', $name);
      Session::put('quantity', $quantity);
      Session::put('price', $price);
      Session::put('arrayIdPricesHab', $arrayIdPricesHab);
      /*Mercado de pago_________________________________*/ 
     
 

      //$mp = new MP("3763616876393218", "yfjYtjoDXWQowRZ9kZSSBNFHD7jgeCpF");
      $mp= new MP($this->CLIENT_ID,$this->CLIENT_SECRET);
      $preference_data = array (
        "items" => array (
          array (
          	"id"=>"12345678",
            "title" => "Test compra posadaparaiso",
            "quantity" => 1,
            "currency_id" => "MX",
            "unit_price" =>  (float)$price
          )
        )
        //,"notification_url" => "http://hotelposadaparaiso.com.mx/notification"
      );
     
      $preference = $mp->create_preference($preference_data);
      


      return redirect()->to($preference['response']['init_point']);
   
      

       //print_r ($preference);
      
      $access_token = $mp->get_access_token();
      //print_r ($access_token);

    }


    public function NotificationPayment(Request $request)
    {
      $mp = new MP($this->CLIENT_ID,$this->CLIENT_SECRET);
         // Get the payment and the corresponding merchant_order reported by the IPN.
        if($_GET["topic"] == 'payment')
        {
          $payment_info = $mp->get("/collections/notifications/" . $_GET["id"]);
          $merchant_order_info = $mp->get("/merchant_orders/" . $payment_info["response"]["collection"]["merchant_order_id"]);
        // Get the merchant_order reported by the IPN.
          //almacenar en bdd
          $this->saveOrder();

          //Creamos el archivo datos.txt
                        //ponemos tipo 'a' para añadir lineas sin borrar
                        $file=fopen("datosPayment.txt","a") or die("Problemas");
                          //vamos añadiendo el contenido
                          fputs($file,$payment_info);
                          fputs($file,"\n");
                          fputs($file,$merchant_order_info);
                          fputs($file,"\n");
                          fputs($file,"tercera linea");
                          fclose($file);

        } 
        else if($_GET["topic"] == 'merchant_order')
        {
          $merchant_order_info = $mp->get("/merchant_orders/" . $_GET["id"]);
        }

        if ($merchant_order_info["status"] == 200) {
          // If the payment's transaction amount is equal (or bigger) than the merchant_order's amount you can release your items 
          $paid_amount = 0;

          foreach ($merchant_order_info["response"]["payments"] as  $payment)
          {
            if ($payment['status'] == 'approved')
            {
              $paid_amount += $payment['transaction_amount'];
            } 
          }

          if($paid_amount >= $merchant_order_info["response"]["total_amount"])
          {
            if(count($merchant_order_info["response"]["shipments"]) > 0)
            { // The merchant_order has shipments
              if($merchant_order_info["response"]["shipments"][0]["status"] == "ready_to_ship")
              {
                print_r("Totally paid. Print the label and release your item.");
              }
            }
            else
            { // The merchant_order don't has any shipments
              print_r("Totally paid. Release your item.");
            }
          }
          else
          {
            print_r("Not paid yet. Do not release your item.");
          }
        }
    }


       protected function saveOrder()
        {
            pp_reservation::create([
            'name'=>Session::get('nombre'),
            'arrival'=>Session::get('llegada'),
            'departure'=>Session::get('salida'),
            'room'=>Session::get('habitacion'),
            'grownups'=>Session::get('adultos'),
            'minors'=>Session::get('menores'),
            'promotions'=>Session::get('promo'),
            'amount'=>Session::get('price'),
            //'id_price'=>Session::get('id_price'),
            ]);
             
            $id_last_inserted = (DB::table('pp_reservation')->max('id'));   


            $arrayPricesId=Session::get('arrayIdPricesHab');
            for($i=0;$i<Session::get('habitacion');$i++){
                 pp_reservation_details::create([
                'id_price'=>$arrayPricesId[$i],
                'id_reservation'=>$id_last_inserted,
                ]);
            }
           


           Session::forget('nombre'); 
           Session::forget('llegada');
           Session::forget('salida');
           Session::forget('habitacion');
           Session::forget('adultos');
           Session::forget('menores');
           Session::forget('promo');
           Session::forget('arrayIdPricesHab');

        }


}


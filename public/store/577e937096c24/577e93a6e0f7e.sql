-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 01-07-2016 a las 10:36:49
-- Versión del servidor: 10.0.24-MariaDB-1~jessie
-- Versión de PHP: 5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `firme_db_sierp_finance`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fin_contract`
--

CREATE TABLE IF NOT EXISTS `fin_contract` (
  `id` varchar(36) NOT NULL,
  `id_adm_organization` varchar(36) DEFAULT NULL,
  `id_adm_client` varchar(36) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `is_private` bit(1) NOT NULL DEFAULT b'0',
  `created_by` varchar(36) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(36) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_credit_account` varchar(36) NOT NULL,
  `id_cat_catalog` varchar(36) DEFAULT NULL,
  `id_cat_periodicity` varchar(36) DEFAULT NULL,
  `id_cat_interest` varchar(36) DEFAULT NULL,
  `id_rate_tax` varchar(36) DEFAULT NULL,
  `id_cat_client_type` varchar(36) DEFAULT NULL,
  `id_provider` varchar(36) DEFAULT NULL,
  `id_product` varchar(36) DEFAULT NULL,
  `id_credit_manager` varchar(36) DEFAULT NULL,
  `label` varchar(500) DEFAULT NULL,
  `description` text,
  `alias1` varchar(500) DEFAULT NULL,
  `alias2` varchar(500) DEFAULT NULL,
  `alias3` varchar(500) DEFAULT NULL,
  `short_code` varchar(10) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `long_code` varchar(30) DEFAULT NULL,
  `client_type_name` varchar(500) DEFAULT NULL,
  `provider_name` varchar(500) DEFAULT NULL,
  `product_name` varchar(500) DEFAULT NULL,
  `credit_manager_name` varchar(500) DEFAULT NULL,
  `catalog_name` varchar(500) DEFAULT NULL,
  `periodicity_value` int(11) DEFAULT NULL,
  `periodicity_type` varchar(500) NOT NULL,
  `interest_type` varchar(500) DEFAULT NULL,
  `rate_tax_name` varchar(500) DEFAULT NULL,
  `total_amount_requested` decimal(18,2) DEFAULT NULL,
  `total_amount_suggested` decimal(18,2) DEFAULT NULL,
  `total_amount_authorized` decimal(18,2) DEFAULT NULL,
  `disbursment_date` date DEFAULT NULL,
  `deliver_date` date DEFAULT NULL,
  `first_payment_date` date DEFAULT NULL,
  `phase` int(11) NOT NULL DEFAULT '0',
  `reca` varchar(30) NOT NULL,
  `last_payment_date` date NOT NULL,
  `account_number` varchar(10) DEFAULT NULL,
  `card_number` varchar(16) DEFAULT NULL,
  `clabe` varchar(18) DEFAULT NULL,
  `period` int(11) DEFAULT NULL,
  `rate_amount` decimal(18,2) DEFAULT NULL,
  `ratetax_amount` decimal(18,2) DEFAULT NULL,
  `interest_amount` decimal(18,2) NOT NULL,
  `tax_amount` decimal(18,2) NOT NULL,
  `translate` text,
  `order_number` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AVG_ROW_LENGTH=963;

--
-- Volcado de datos para la tabla `fin_contract`
--

INSERT INTO `fin_contract` (`id`, `id_adm_organization`, `id_adm_client`, `is_active`, `is_private`, `created_by`, `created_at`, `updated_by`, `updated_at`, `id_credit_account`, `id_cat_catalog`, `id_cat_periodicity`, `id_cat_interest`, `id_rate_tax`, `id_cat_client_type`, `id_provider`, `id_product`, `id_credit_manager`, `label`, `description`, `alias1`, `alias2`, `alias3`, `short_code`, `code`, `long_code`, `client_type_name`, `provider_name`, `product_name`, `credit_manager_name`, `catalog_name`, `periodicity_value`, `periodicity_type`, `interest_type`, `rate_tax_name`, `total_amount_requested`, `total_amount_suggested`, `total_amount_authorized`, `disbursment_date`, `deliver_date`, `first_payment_date`, `phase`, `reca`, `last_payment_date`, `account_number`, `card_number`, `clabe`, `period`, `rate_amount`, `ratetax_amount`, `interest_amount`, `tax_amount`, `translate`, `order_number`, `status`) VALUES
('ff8081815591f381015591f5b4ca0004', NULL, 'N/A', b'1', b'0', NULL, '2016-06-27 13:04:06', NULL, '2016-07-01 11:25:56', 'ff8081815591f381015591f4cfce0000', 'ff80818154a9b06b0154a9b4f64e0000', 'ff80818154a97b380154a97ff6df0005', 'ff80818154a97b380154a981d3b00009', 'ff80818154f0d9680154f356579e000b', 'ff80818154a97b380154a98416f7000c', 'ff80818154f0d9680154f3540c4b000a', 'ff80818154f0d9680154f366f56a000e', 'ff80818154ff4fa90154ffa58294001a', 'ROMANES', NULL, NULL, NULL, NULL, NULL, '1', NULL, 'Individuales', 'FIRME SOLUCIONES SA DE CV', 'IND 12 SEM 50', 'Felipe Antonio Roman Albores', 'Cuenta de Credito', 0, 'PER_SEM', 'Saldos Globales', 'IVA 0', 12000.00, 12000.00, 12000.00, '2016-06-27', '2016-06-27', '2016-07-04', 0, '', '2016-07-04', NULL, NULL, NULL, 12, 50.00, 0.00, 0.00, 0.00, NULL, 1, 'PROCESS'),
('ff8081815591f38101559249303b00e6', NULL, 'N/A', b'1', b'0', NULL, '2016-06-27 14:35:17', NULL, '2016-07-01 11:25:42', 'ff8081815591f38101559246d4a200e1', 'ff80818154a9b06b0154a9b4f64e0000', 'ff80818154a97b380154a98062bc0006', 'ff80818154a97b380154a981d3b00009', 'ff80818154fef0200154fef2ce640000', 'ff80818154a97b380154a983ae28000b', 'ff80818154f0d9680154f3540c4b000a', 'ff80818154fef0200154fef40ef00001', 'ff80818154ff4fa90154ffa58294001a', 'CONEJAS', NULL, NULL, NULL, NULL, NULL, '2', NULL, 'Grupo Solidario', 'FIRME SOLUCIONES SA DE CV', 'PRODUCTO IMP 16', 'Felipe Antonio Roman Albores', 'Cuenta de Credito', 0, 'PER_CAT', 'Saldos Globales', 'IVA 16', 14500.00, 14500.00, 14500.00, '2016-06-20', '2016-06-27', '2016-07-04', 0, '', '2016-07-04', NULL, NULL, NULL, 10, 50.00, 16.00, 0.00, 0.00, NULL, 2, 'PROCESS'),
('ff80818155a42ada0155a436c2cb0004', NULL, 'N/A', b'1', b'0', NULL, '2016-07-01 02:08:20', NULL, '2016-07-01 11:25:48', 'ff80818155a42ada0155a435ab480000', 'ff80818154a9b06b0154a9b4f64e0000', 'ff80818154a97b380154a97ff6df0005', 'ff80818154a97b380154a981d3b00009', 'ff80818154f0d9680154f356579e000b', 'ff80818154a97b380154a98416f7000c', 'ff80818154f0d9680154f3540c4b000a', 'ff80818154f0d9680154f366f56a000e', 'ff80818154ff4fa90154ffa58294001a', 'Prueba de Calendario ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Individuales', 'FIRME SOLUCIONES SA DE CV', 'IND 12 SEM 50', 'Felipe Antonio Roman Albores', 'Cuenta de Credito', 0, 'Semanal', 'Saldos Globales', 'IVA 0', 14000.00, 14000.00, 14000.00, '2016-05-30', '2016-06-01', '2016-06-06', 0, '', '2016-06-06', NULL, NULL, NULL, 12, 50.00, 0.00, 0.00, 0.00, NULL, 3, 'PROCESS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fin_contract_detail`
--

CREATE TABLE IF NOT EXISTS `fin_contract_detail` (
  `id` varchar(36) NOT NULL,
  `id_adm_organization` varchar(36) DEFAULT NULL,
  `id_adm_client` varchar(36) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `is_private` bit(1) NOT NULL DEFAULT b'0',
  `created_by` varchar(36) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(36) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_contract` varchar(36) NOT NULL,
  `id_credit_account_detail` varchar(36) NOT NULL,
  `id_client` varchar(36) DEFAULT NULL,
  `id_cat_client_type` varchar(36) DEFAULT NULL,
  `id_provider` varchar(36) DEFAULT NULL,
  `client_name` varchar(500) DEFAULT NULL,
  `client_type_name` varchar(500) DEFAULT NULL,
  `provider_name` varchar(500) DEFAULT NULL,
  `total_amount_requested` decimal(18,2) DEFAULT NULL,
  `total_amount_suggested` decimal(18,2) DEFAULT NULL,
  `total_amount_authorized` decimal(18,2) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `phase` int(11) DEFAULT '0',
  `short_code` varchar(10) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `long_code` varchar(30) DEFAULT NULL,
  `description` text,
  `translate` text,
  `order_number` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AVG_ROW_LENGTH=963;

--
-- Volcado de datos para la tabla `fin_contract_detail`
--

INSERT INTO `fin_contract_detail` (`id`, `id_adm_organization`, `id_adm_client`, `is_active`, `is_private`, `created_by`, `created_at`, `updated_by`, `updated_at`, `id_contract`, `id_credit_account_detail`, `id_client`, `id_cat_client_type`, `id_provider`, `client_name`, `client_type_name`, `provider_name`, `total_amount_requested`, `total_amount_suggested`, `total_amount_authorized`, `position`, `phase`, `short_code`, `code`, `long_code`, `description`, `translate`, `order_number`, `status`) VALUES
('ff8081815591f381015591f5b5040006', NULL, 'N/A', b'1', b'0', NULL, '2016-06-27 13:04:06', NULL, '2016-06-27 13:04:06', 'ff8081815591f381015591f5b4ca0004', 'ff8081815591f381015591f517c40001', 'ff808181553bb59d01553bbb672e0002', NULL, NULL, 'Flor de Margarita Nucamendi Gonzalez', '', '', 3000.00, 3000.00, 3000.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL),
('ff8081815591f381015591f5b5160007', NULL, 'N/A', b'1', b'0', NULL, '2016-06-27 13:04:06', NULL, '2016-06-27 13:04:06', 'ff8081815591f381015591f5b4ca0004', 'ff8081815591f381015591f54e230002', 'ff80818155900c9d015590505600000a', NULL, NULL, 'Juana Hernandez Grajales', '', '', 5000.00, 5000.00, 5000.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, 2, NULL),
('ff8081815591f381015591f5b52d0008', NULL, 'N/A', b'1', b'0', NULL, '2016-06-27 13:04:06', NULL, '2016-06-27 13:04:06', 'ff8081815591f381015591f5b4ca0004', 'ff8081815591f381015591f58be90003', 'ff80818155900c9d0155904fcfa90008', NULL, NULL, 'Felipe Antonio Roman Albores Roman Albores', '', '', 4000.00, 4000.00, 4000.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, 3, NULL),
('ff8081815591f38101559249307500e9', NULL, 'N/A', b'1', b'0', NULL, '2016-06-27 14:35:18', NULL, '2016-06-27 14:35:18', 'ff8081815591f38101559249303b00e6', 'ff8081815591f381015592479a1e00e3', 'ff808181553bb59d01553bbb672e0002', NULL, NULL, 'Flor de Margarita Nucamendi Gonzalez', '', '', 4000.00, 4000.00, 4000.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, NULL),
('ff8081815591f38101559249309c00ea', NULL, 'N/A', b'1', b'0', NULL, '2016-06-27 14:35:18', NULL, '2016-06-27 14:35:18', 'ff8081815591f38101559249303b00e6', 'ff8081815591f3810155924754b200e2', 'ff80818155900c9d0155904fcfa90008', NULL, NULL, 'Felipe Antonio Roman Albores Roman Albores', '', '', 2500.00, 2500.00, 2500.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, 5, NULL),
('ff8081815591f3810155924930a500eb', NULL, 'N/A', b'1', b'0', NULL, '2016-06-27 14:35:18', NULL, '2016-06-27 14:35:18', 'ff8081815591f38101559249303b00e6', 'ff8081815591f38101559247e28100e4', 'ff80818155900c9d0155904d5f690006', NULL, NULL, 'Juan Grajales Grajales', '', '', 5000.00, 5000.00, 5000.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, 6, NULL),
('ff8081815591f3810155924930ac00ec', NULL, 'N/A', b'1', b'0', NULL, '2016-06-27 14:35:18', NULL, '2016-06-27 14:35:18', 'ff8081815591f38101559249303b00e6', 'ff8081815591f3810155924825ba00e5', 'ff80818155900c9d015590505600000a', NULL, NULL, 'Juana Hernandez Grajales', '', '', 3000.00, 3000.00, 3000.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, 7, NULL),
('ff80818155a42ada0155a436c3210006', NULL, 'N/A', b'1', b'0', NULL, '2016-07-01 02:08:20', NULL, '2016-07-01 02:08:20', 'ff80818155a42ada0155a436c2cb0004', 'ff80818155a42ada0155a435ee090001', 'ff80818155900c9d0155904fcfa90008', NULL, NULL, 'Felipe Antonio Roman Albores Roman Albores', '', '', 5000.00, 5000.00, 5000.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, 8, NULL),
('ff80818155a42ada0155a436c3300007', NULL, 'N/A', b'1', b'0', NULL, '2016-07-01 02:08:20', NULL, '2016-07-01 02:08:20', 'ff80818155a42ada0155a436c2cb0004', 'ff80818155a42ada0155a4362b210002', 'ff808181553bb59d01553bbb672e0002', NULL, NULL, 'Flor de Margarita Nucamendi Gonzalez', '', '', 3000.00, 3000.00, 3000.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, 9, NULL),
('ff80818155a42ada0155a436c3490008', NULL, 'N/A', b'1', b'0', NULL, '2016-07-01 02:08:20', NULL, '2016-07-01 02:08:20', 'ff80818155a42ada0155a436c2cb0004', 'ff80818155a42ada0155a436744d0003', 'ff80818155900c9d015590505600000a', NULL, NULL, 'Juana Hernandez Grajales', '', '', 6000.00, 6000.00, 6000.00, NULL, 0, NULL, NULL, NULL, NULL, NULL, 10, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fin_credit_account`
--

CREATE TABLE IF NOT EXISTS `fin_credit_account` (
  `id` varchar(36) NOT NULL,
  `id_parent` varchar(36) DEFAULT NULL,
  `id_adm_organization` varchar(36) DEFAULT NULL,
  `id_adm_client` varchar(36) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `is_private` bit(1) NOT NULL DEFAULT b'0',
  `created_by` varchar(36) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(36) DEFAULT NULL,
  `id_cat_client_type` varchar(36) DEFAULT NULL,
  `id_provider` varchar(36) DEFAULT NULL,
  `id_product` varchar(36) DEFAULT NULL,
  `id_credit_manager` varchar(36) DEFAULT NULL,
  `total_amount_requested` decimal(18,2) DEFAULT NULL,
  `total_amount_suggested` decimal(18,2) DEFAULT NULL,
  `total_amount_authorized` decimal(18,2) DEFAULT NULL,
  `disbursment_date` date DEFAULT NULL,
  `deliver_date` date DEFAULT NULL,
  `first_payment_date` date DEFAULT NULL,
  `phase` int(11) NOT NULL DEFAULT '0',
  `account_numbre` varchar(10) DEFAULT NULL,
  `card_number` varchar(16) DEFAULT NULL,
  `clabe` varchar(18) DEFAULT NULL,
  `alias` varchar(500) DEFAULT NULL,
  `description` text,
  `translate` text,
  `order_number` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AVG_ROW_LENGTH=963;

--
-- Volcado de datos para la tabla `fin_credit_account`
--

INSERT INTO `fin_credit_account` (`id`, `id_parent`, `id_adm_organization`, `id_adm_client`, `is_active`, `is_private`, `created_by`, `created_at`, `updated_at`, `updated_by`, `id_cat_client_type`, `id_provider`, `id_product`, `id_credit_manager`, `total_amount_requested`, `total_amount_suggested`, `total_amount_authorized`, `disbursment_date`, `deliver_date`, `first_payment_date`, `phase`, `account_numbre`, `card_number`, `clabe`, `alias`, `description`, `translate`, `order_number`, `status`) VALUES
('ff8081815591f381015591f4cfce0000', '', NULL, 'N/A', b'1', b'0', 'ff80818154f30b030154f31584ab0000', '2016-06-27 13:03:08', '2016-06-27 05:00:00', 'ff80818154f30b030154f31584ab0000', NULL, 'ff80818154f0d9680154f3540c4b000a', 'ff80818154f0d9680154f366f56a000e', 'ff80818154ff4fa90154ffa58294001a', 12000.00, 12000.00, 12000.00, '2016-06-27', '2016-06-27', '2016-07-04', 0, NULL, NULL, NULL, 'ROMANES', '', NULL, 1, 'AUTHORIZED'),
('ff8081815591f38101559246d4a200e1', '', NULL, 'N/A', b'1', b'0', 'ff80818154f30b030154f31584ab0000', '2016-06-27 14:32:43', '2016-06-27 05:00:00', 'ff80818154f30b030154f31584ab0000', NULL, 'ff80818154f0d9680154f3540c4b000a', 'ff80818154fef0200154fef40ef00001', 'ff80818154ff4fa90154ffa58294001a', 14500.00, 14500.00, 14500.00, '2016-06-20', '2016-06-27', '2016-07-04', 0, NULL, NULL, NULL, 'CONEJAS', '', NULL, 2, 'AUTHORIZED'),
('ff80818155a42ada0155a435ab480000', '', NULL, 'N/A', b'1', b'0', 'ff80818154f30b030154f31584ab0000', '2016-07-01 02:07:08', '2016-06-30 05:00:00', 'ff80818154f30b030154f31584ab0000', NULL, 'ff80818154f0d9680154f3540c4b000a', 'ff80818154f0d9680154f366f56a000e', 'ff80818154ff4fa90154ffa58294001a', 14000.00, 14000.00, 14000.00, '2016-05-30', '2016-06-01', '2016-06-06', 0, NULL, NULL, NULL, 'Prueba de Calendario ', '', NULL, 3, 'AUTHORIZED');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fin_credit_account_detail`
--

CREATE TABLE IF NOT EXISTS `fin_credit_account_detail` (
  `id` varchar(36) NOT NULL,
  `id_credit_account` varchar(36) NOT NULL,
  `id_client` varchar(36) DEFAULT NULL,
  `id_adm_organization` varchar(36) DEFAULT NULL,
  `id_adm_client` varchar(36) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `is_private` bit(1) NOT NULL DEFAULT b'0',
  `created_by` varchar(36) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(36) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cat_client_type` varchar(36) DEFAULT NULL,
  `id_provider` varchar(36) DEFAULT NULL,
  `total_amount_requested` decimal(18,2) DEFAULT NULL,
  `total_amount_suggested` decimal(18,2) DEFAULT NULL,
  `total_amount_authorized` decimal(18,2) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `short_code` varchar(10) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `long_code` varchar(30) DEFAULT NULL,
  `description` text,
  `translate` text,
  `order_number` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AVG_ROW_LENGTH=963;

--
-- Volcado de datos para la tabla `fin_credit_account_detail`
--

INSERT INTO `fin_credit_account_detail` (`id`, `id_credit_account`, `id_client`, `id_adm_organization`, `id_adm_client`, `is_active`, `is_private`, `created_by`, `created_at`, `updated_by`, `updated_at`, `id_cat_client_type`, `id_provider`, `total_amount_requested`, `total_amount_suggested`, `total_amount_authorized`, `position`, `short_code`, `code`, `long_code`, `description`, `translate`, `order_number`, `status`) VALUES
('ff8081815591f381015591f517c40001', 'ff8081815591f381015591f4cfce0000', 'ff808181553bb59d01553bbb672e0002', NULL, NULL, b'1', b'1', NULL, '2016-06-27 13:03:26', NULL, '2016-06-27 13:03:26', NULL, NULL, 3000.00, 3000.00, 3000.00, NULL, NULL, NULL, NULL, '', NULL, 1, NULL),
('ff8081815591f381015591f54e230002', 'ff8081815591f381015591f4cfce0000', 'ff80818155900c9d015590505600000a', NULL, NULL, b'1', b'1', NULL, '2016-06-27 13:03:40', NULL, '2016-06-27 13:03:40', NULL, NULL, 5000.00, 5000.00, 5000.00, NULL, NULL, NULL, NULL, '', NULL, 2, NULL),
('ff8081815591f381015591f58be90003', 'ff8081815591f381015591f4cfce0000', 'ff80818155900c9d0155904fcfa90008', NULL, NULL, b'1', b'1', NULL, '2016-06-27 13:03:56', NULL, '2016-06-27 13:03:56', NULL, NULL, 4000.00, 4000.00, 4000.00, NULL, NULL, NULL, NULL, '', NULL, 3, NULL),
('ff8081815591f3810155924754b200e2', 'ff8081815591f38101559246d4a200e1', 'ff80818155900c9d0155904fcfa90008', NULL, NULL, b'1', b'1', NULL, '2016-06-27 14:33:16', NULL, '2016-06-27 14:33:16', NULL, NULL, 2500.00, 2500.00, 2500.00, NULL, NULL, NULL, NULL, '', NULL, 4, NULL),
('ff8081815591f381015592479a1e00e3', 'ff8081815591f38101559246d4a200e1', 'ff808181553bb59d01553bbb672e0002', NULL, NULL, b'1', b'1', NULL, '2016-06-27 14:33:33', NULL, '2016-06-27 14:33:40', NULL, NULL, 4000.00, 4000.00, 4000.00, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL),
('ff8081815591f38101559247e28100e4', 'ff8081815591f38101559246d4a200e1', 'ff80818155900c9d0155904d5f690006', NULL, NULL, b'1', b'1', NULL, '2016-06-27 14:33:52', NULL, '2016-06-27 14:33:52', NULL, NULL, 5000.00, 5000.00, 5000.00, NULL, NULL, NULL, NULL, '', NULL, 6, NULL),
('ff8081815591f3810155924825ba00e5', 'ff8081815591f38101559246d4a200e1', 'ff80818155900c9d015590505600000a', NULL, NULL, b'1', b'1', NULL, '2016-06-27 14:34:09', NULL, '2016-06-27 14:34:09', NULL, NULL, 3000.00, 3000.00, 3000.00, NULL, NULL, NULL, NULL, '', NULL, 7, NULL),
('ff80818155a42ada0155a435ee090001', 'ff80818155a42ada0155a435ab480000', 'ff80818155900c9d0155904fcfa90008', NULL, NULL, b'1', b'1', NULL, '2016-07-01 02:07:25', NULL, '2016-07-01 02:07:25', NULL, NULL, 5000.00, 5000.00, 5000.00, NULL, NULL, NULL, NULL, '', NULL, 8, NULL),
('ff80818155a42ada0155a4362b210002', 'ff80818155a42ada0155a435ab480000', 'ff808181553bb59d01553bbb672e0002', NULL, NULL, b'1', b'1', NULL, '2016-07-01 02:07:41', NULL, '2016-07-01 02:07:41', NULL, NULL, 3000.00, 3000.00, 3000.00, NULL, NULL, NULL, NULL, '', NULL, 9, NULL),
('ff80818155a42ada0155a436744d0003', 'ff80818155a42ada0155a435ab480000', 'ff80818155900c9d015590505600000a', NULL, NULL, b'1', b'1', NULL, '2016-07-01 02:08:00', NULL, '2016-07-01 02:08:00', NULL, NULL, 6000.00, 6000.00, 6000.00, NULL, NULL, NULL, NULL, '', NULL, 10, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `fin_contract`
--
ALTER TABLE `fin_contract`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indices de la tabla `fin_contract_detail`
--
ALTER TABLE `fin_contract_detail`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_UNIQUE` (`id`), ADD KEY `FK_fin_contract_detail_fin_contract_id` (`id_contract`);

--
-- Indices de la tabla `fin_credit_account`
--
ALTER TABLE `fin_credit_account`
 ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indices de la tabla `fin_credit_account_detail`
--
ALTER TABLE `fin_credit_account_detail`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_UNIQUE` (`id`), ADD KEY `FK_fin_credit_account_detail_fin_credit_account_id` (`id_credit_account`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
